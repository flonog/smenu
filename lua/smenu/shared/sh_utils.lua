SMenu = SMenu or {}
SMenu.utils = SMenu.utils or {}

function SMenu.utils.LerpColor(t, from, to)

	local r = Lerp(t, from["r"], to["r"])
	local g = Lerp(t, from["g"], to["g"])
	local b = Lerp(t, from["b"], to["b"])
	return  Color(r,g,b)

end

function SMenu.utils.__genOrderedIndex( t )
    local orderedIndex = {}
    for key in pairs(t) do
        table.insert( orderedIndex, key )
    end
    table.sort( orderedIndex )
    return orderedIndex
end

function SMenu.utils.orderedNext(t, state)

    local key = nil
    if state == nil then
        t.__orderedIndex = SMenu.utils.__genOrderedIndex( t )
        key = t.__orderedIndex[1]
    else
        for i = 1,table.getn(t.__orderedIndex) do
            if t.__orderedIndex[i] == state then
                key = t.__orderedIndex[i+1]
            end
        end
    end

    if key then
        return key, t[key]
    end

    t.__orderedIndex = nil
    return
end

function SMenu.utils.orderedPairs(t)
    return SMenu.utils.orderedNext, t, nil
end
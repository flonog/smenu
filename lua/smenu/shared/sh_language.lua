SMenu = SMenu or {}

if !SMenu.Config.Language then
	print("[LANG] Error, no language defined.")
	return
end

include("smenu/language/" .. SMenu.Config.Language .. ".lua")
print("[LANG] " .. SMenu.Config.Language .. " selected and loaded.")
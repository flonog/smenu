local PL = FindMetaTable("Player")

function PL:IsAllowed(name)

	if table.HasValue(SMenu.Config.IgnoreToolVerification, self:GetUserGroup()) then
		return true
	end

	
	local toolConfig = SMenu.ConfigManager.GetConfig("toolgun")

	if toolConfig.value[name] == nil then
		return false
	end

	return table.HasValue(toolConfig.value[name], self:GetUserGroup()) || table.HasValue(toolConfig.value[name], "*")
end
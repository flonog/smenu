SMenu = SMenu or {}
SMenu.ConfigManager = SMenu.ConfigManager or {}
SMenu.ConfigManager.ServerConfig = SMenu.ConfigManager.ServerConfig or {}
local configPath = "smenu/config.json"

function SMenu.ConfigManager.AddConfig(id, category, name, description, defaultvalue)
	if SMenu.ConfigManager.ServerConfig[id] != nil then
		SMenu.ConfigManager.ServerConfig[id].name = name
		SMenu.ConfigManager.ServerConfig[id].category = category
		SMenu.ConfigManager.ServerConfig[id].description = description
		SMenu.ConfigManager.ServerConfig[id].defaultvalue = defaultvalue
		return
	end
	SMenu.ConfigManager.ServerConfig[id] = {
		name = name,
		category = category,
		description = description,
		defaultvalue = defaultvalue,
		value = defaultvalue,
	}
end

function SMenu.ConfigManager.GetConfig(id)
	return SMenu.ConfigManager.ServerConfig[id]
end

function SMenu.ConfigManager.Save()
	SMenu.ConfigManager.CheckDirectory()
	file.Write(configPath, util.TableToJSON(SMenu.ConfigManager.ServerConfig, true))
end

function SMenu.ConfigManager.ReloadServer()
	SMenu.ConfigManager.CheckDirectory()
	if not file.Exists(configPath, "DATA") then
		file.Write(configPath, "")
		SMenu.ConfigManager.ServerConfig = {}
		return
	end

	local jsonConfig = file.Read(configPath, "DATA")
	SMenu.ConfigManager.ServerConfig = util.JSONToTable(jsonConfig)
end

function SMenu.ConfigManager.CheckDirectory()
	if not file.Exists("smenu", "DATA") then
		file.CreateDir("smenu")
	end
end

function SMenu.ConfigManager.ForceReload()
	net.Start("SMenu:Config")
	local tableParam = {action = "reload", config = SMenu.ConfigManager.ServerConfig}
	local tableJson = util.TableToJSON(tableParam)
	local compressed = util.Compress(tableJson)
	net.WriteData(compressed, #compressed)
	net.Broadcast()
end

function SMenu.ConfigManager.ForceReloadPlayer(ply)
	net.Start("SMenu:Config")
	local tableParam = {action = "reload", config = SMenu.ConfigManager.ServerConfig}
	local tableJson = util.TableToJSON(tableParam)
	local compressed = util.Compress(tableJson)
	net.WriteData(compressed, #compressed)
	net.Send(ply)
end

net.Receive("SMenu:Config", function(len, ply)
	local compressTable = net.ReadData(len/8)
	local tableJson = util.Decompress(compressTable)
	local rtable = util.JSONToTable(tableJson)
	local type = rtable.action

	if type == "get" then
		net.Start("SMenu:Config")
		local tableParam = {action = "reload", config = SMenu.ConfigManager.ServerConfig}
		local tableJson = util.TableToJSON(tableParam)
		local compressed = util.Compress(tableJson)
		net.WriteData(compressed, #compressed)
		net.Send(ply)
	elseif type == "set" then
		if not table.HasValue(SMenu.Config.AllowedGroup, ply:GetUserGroup()) then
			return
		end
		SMenu.ConfigManager.ServerConfig[rtable.id].value = rtable.value
		SMenu.ConfigManager.Save()
		SMenu.ConfigManager.ForceReload()
	end
end)
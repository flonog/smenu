if SMenu.Config.FastDl then

	resource.AddSingleFile("materials/smenu/add.png")
	resource.AddSingleFile("materials/smenu/edit.png")
	resource.AddSingleFile("materials/smenu/entities.png")
	resource.AddSingleFile("materials/smenu/oldmenu_32.png")
	resource.AddSingleFile("materials/smenu/oldmenu.png")
	resource.AddSingleFile("materials/smenu/options_32.png")
	resource.AddSingleFile("materials/smenu/options.png")
	resource.AddSingleFile("materials/smenu/props.png")
	resource.AddSingleFile("materials/smenu/toolgun.png")
	resource.AddSingleFile("materials/smenu/vehicles.png")
	resource.AddSingleFile("materials/smenu/weapons.png")

end
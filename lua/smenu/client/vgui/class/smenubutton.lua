local PANEL = {}

function PANEL:Init()

	self:SetContentAlignment( 5 )

	self:SetDrawBorder( true )
	self:SetPaintBackground( true )

	self:SetTall( 22 )
	self:SetMouseInputEnabled( true )
	self:SetKeyboardInputEnabled( true )

	self:SetCursor( "hand" )
	self:SetFont( "SMenu:Title" )
	self.HoverColor = SMenu.Color.GetColor("accent")
	self.Color = SMenu.Color.GetColor("black")
	self.ActualColor = SMenu.Color.GetColor("black")

end

function PANEL:Paint(w,h)

	if self.Depressed then
		self.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), self.ActualColor, SMenu.Color.GetColor("primary"))
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
		return
	end

	if self:IsHovered() then
		self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, self.HoverColor)
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
	else 
		self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, self.Color)
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
	end
	if self:GetToggle() || self.m_bSelected then
		draw.RoundedBox(0, 0, 0, w, h, self.HoverColor)
	end
	
end

function PANEL:UpdateColours( skin )

	return self:SetTextColor(SMenu.Color.GetColor("white"))

end

function PANEL:PerformLayout()
	if ( IsValid( self.m_Image ) ) then

		if(self:GetText() == "") then
			self.m_Image:SetPos((self:GetSize() - self.m_Image:GetSize()) * 0.4, ( self:GetTall() - self.m_Image:GetTall() ) * 0.5 )
		else 
			self.m_Image:SetPos(4, ( self:GetTall() - self.m_Image:GetTall() ) * 0.5 )
			self:SetTextInset( self.m_Image:GetWide() + 16, 0 )
		end
	end

	

	DLabel.PerformLayout( self )

end

vgui.Register("SMenuButton", PANEL, "DButton")
local PANEL = {}

local border = 0
local border_w = 8
local matHover = Material( "gui/ps_hover.png", "nocull" )
local boxHover = GWEN.CreateTextureBorder( border, border, 64 - border * 2, 64 - border * 2, border_w, border_w, border_w, border_w, matHover )

local function HighlightedButtonPaint( self, w, h )

	boxHover( 0, 0, w, h, color_white )

end

function PANEL:Init()
	self.List = vgui.Create( "SPanelList", self )
	self.List:EnableHorizontal( true )
	self.List:EnableVerticalScrollbar()
	self.List:SetSpacing( 1 )
	self.List:SetPadding( 3 )

	Derma_Hook( self.List, "Paint", "Paint", "Panel" )

	self.Controls = {}
	self.Height = 2

end

function PANEL:Paint(w, h)

end

function PANEL:AddModel( model, ConVars )

	local Icon = vgui.Create( "SpawnIcon", self )
	Icon:SetModel( model )
	Icon:SetTooltip( model )
	Icon.Model = model
	Icon.ConVars = ConVars || {}

	local ConVarName = self:ConVar()

	Icon.DoClick = function ( self )

		for k, v in pairs( self.ConVars ) do
			LocalPlayer():ConCommand( Format( "%s \"%s\"\n", k, v ) )
		end

		LocalPlayer():ConCommand( Format( "%s \"%s\"\n", ConVarName, model ) )

	end
	Icon.OpenMenu = function( button )
		local menu = DermaMenu()
		menu:AddOption( "#spawnmenu.menu.copy", function() SetClipboardText( model ) end ):SetIcon( "icon16/page_copy.png" )
		menu:Open()
	end

	self.List:AddItem( Icon )
	table.insert( self.Controls, Icon )

end

function PANEL:AddModelEx( name, model, skin )

	local Icon = vgui.Create( "SpawnIcon", self )
	Icon:SetModel( model, skin )
	Icon:SetTooltip( model )
	Icon.Model = model
	Icon.Value = name
	Icon.ConVars = ConVars || {}

	local ConVarName = self:ConVar()

	Icon.DoClick = function ( self ) LocalPlayer():ConCommand( Format( "%s \"%s\"\n", ConVarName, Icon.Value ) ) end
	Icon.OpenMenu = function( button )
		local menu = DermaMenu()
		menu:AddOption( "Copy to Clipboard", function() SetClipboardText( model ) end ):SetIcon( "icon16/page_copy.png" )
		menu:Open()
	end

	self.List:AddItem( Icon )
	table.insert( self.Controls, Icon )

end

function PANEL:ControlValues( kv )

	self.BaseClass.ControlValues( self, kv )

	self.Height = kv.height || 2

	if ( kv.models ) then
		for k, v in SortedPairs( kv.models ) do
			self:AddModel( k, v )
		end
	end

	if ( kv.modelstable ) then
		local tmp = {}
		for k, v in SortedPairsByMemberValue( kv.modelstable, "model" ) do
			tmp[ k ] = v.model .. ( v.skin || 0 )
		end

		for k, v in SortedPairsByValue( tmp ) do
			v = kv.modelstable[ k ]
			self:AddModelEx( k, v.model, v.skin || 0 )
		end
	end

	self:InvalidateLayout( true )

end

function PANEL:PerformLayout()

	local y = self.BaseClass.PerformLayout( self )

	if ( self.Height >= 1 ) then
		local Height = ( 64 + self.List:GetSpacing() ) * math.max( self.Height, 1 ) + self.List:GetPadding() * 2 - self.List:GetSpacing()

		self.List:SetPos( 0, y )
		self.List:SetSize( self:GetWide(), Height )

		y = y + Height

		self:SetTall( y + 5 )
	else
		self.List:SetWide( self:GetWide() )
		self.List:SizeToChildren( false, true )
		self:SetTall( self.List:GetTall() + 5 )
	end

end

function PANEL:FindAndSelectButton( Value )

	self.CurrentValue = Value

	for k, Icon in pairs( self.Controls ) do

		if ( Icon.Model == Value || Icon.Value == Value ) then

			if ( self.SelectedIcon ) then
				self.SelectedIcon.PaintOver = self.OldSelectedPaintOver
			end

			self.OldSelectedPaintOver = Icon.PaintOver
			Icon.PaintOver = HighlightedButtonPaint
			self.SelectedIcon = Icon

		end

	end

end

function PANEL:TestForChanges()

	local Value = GetConVarString( self:ConVar() )

	if ( Value == self.CurrentValue ) then return end

	self:FindAndSelectButton( Value )

end

vgui.Register( "SPropSelect", PANEL, "ContextBase" )
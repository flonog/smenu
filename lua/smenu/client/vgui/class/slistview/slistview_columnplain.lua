local PANEL = {}

function PANEL:Init()

	self.Header = vgui.Create( "SMenuButton", self )
	self.Header.DoClick = function() self:DoClick() end
	self.Header.DoRightClick = function() self:DoRightClick() end
	self.Header:SetFont("SMenu:Text")

	self.DraggerBar = vgui.Create( "DListView_DraggerBar", self )

	self:SetMinWidth( 10 )
	self:SetMaxWidth( 19200 )

end

vgui.Register("SListView_ColumnPlain", PANEL, "DListView_ColumnPlain")
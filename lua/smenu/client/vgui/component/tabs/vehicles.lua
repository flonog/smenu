SMenu.frame = SMenu.frame or {}
SMenu.frame.vehicles = SMenu.frame.vehicles or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frameFunc.vehicles = SMenu.frameFunc.vehicles or {}
SMenu.frame.vehicles_lastSelect = ""

function SMenu.frameFunc.vehicles.Show()

	local mpx, mpy = SMenu.frame.main_panel:GetSize()

	SMenu.frame.vehicles.categoryIconBrowser = vgui.Create("SIconBrowser", SMenu.frame.main_panel)
	SMenu.frame.vehicles.categoryIconBrowser:SetSize(mpx - mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.vehicles.categoryIconBrowser:SetPos(mpx * 0.3, mpy * 0.075)
	function SMenu.frame.vehicles.categoryIconBrowser:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	end

	SMenu.frame.vehicles.categoryScrollPanel = vgui.Create("SScrollPanel", SMenu.frame.main_panel)
	SMenu.frame.vehicles.categoryScrollPanel:SetSize(mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.vehicles.categoryScrollPanel:SetPos(0, mpy * 0.075)
	function SMenu.frame.vehicles.categoryScrollPanel:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end

	local csx, csy = SMenu.frame.vehicles.categoryScrollPanel:GetSize()
	local c = 0
	for k,v in SMenu.utils.orderedPairs(SMenu.entStorage.Vehicles) do
		c = c + 1
		local button = SMenu.frame.vehicles.categoryScrollPanel:Add("SMenuButton")
		button:Dock(TOP)
		button:SetSize(csx, 75)
		button:SetText(k)
		button.Color = SMenu.Color.GetColor("secondary")
		if SMenu.frame.vehicles_lastSelect == v || (c == 1 && SMenu.frame.vehicles_lastSelect == "") then
			button:SetToggle(true)
			hasToggle = true
			SMenu.frameFunc.vehicles.SetVehicles(v)
		end
		function button:DoClick()
			for _,v in pairs(SMenu.frame.vehicles.categoryScrollPanel:GetCanvas():GetChildren()) do
				v:SetToggle(false)
			end
			button:SetToggle(true)
			SMenu.frame.vehicles_lastSelect = v
			SMenu.frameFunc.vehicles.SetVehicles(v)
		end
	end
end

function SMenu.frameFunc.vehicles.Close()
	if IsValid(SMenu.frame.vehicles.categoryIconBrowser) then
		SMenu.frame.vehicles.categoryIconBrowser:Remove()
	end

	if IsValid(SMenu.frame.vehicles.categoryScrollPanel) then
		SMenu.frame.vehicles.categoryScrollPanel:Remove()
	end
end

function SMenu.frameFunc.vehicles.SetVehicles(vehicles)
	SMenu.frame.vehicles.categoryIconBrowser:Clear()
	for _, v in pairs(vehicles) do
		local icon = SMenu.frame.vehicles.categoryIconBrowser:Add("ContentIcon")
		icon:SetContentType( "vehicle" )
		icon:SetSpawnName( v.ClassName )
		icon:SetName( v.Name )
		icon:SetMaterial( "entities/" .. v.ClassName .. ".png" )
		icon:SetAdminOnly( v.AdminOnly or false )
		function icon:DoClick()
			surface.PlaySound("ui/buttonclickrelease.wav")
			RunConsoleCommand( "gm_spawnvehicle", v.ClassName )
		end
	end
end
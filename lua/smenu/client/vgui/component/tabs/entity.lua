SMenu.frame = SMenu.frame or {}
SMenu.frame.entities = SMenu.frame.entities or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frameFunc.entities = SMenu.frameFunc.entities or {}
SMenu.frame.entities_lastSelect = ""
function SMenu.frameFunc.entities.Show()

	local mpx, mpy = SMenu.frame.main_panel:GetSize()

	SMenu.frame.entities.categoryIconBrowser = vgui.Create("SIconBrowser", SMenu.frame.main_panel)
	SMenu.frame.entities.categoryIconBrowser:SetSize(mpx - mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.entities.categoryIconBrowser:SetPos(mpx * 0.3, mpy * 0.075)
	function SMenu.frame.entities.categoryIconBrowser:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	end

	SMenu.frame.entities.categoryScrollPanel = vgui.Create("SScrollPanel", SMenu.frame.main_panel)
	SMenu.frame.entities.categoryScrollPanel:SetSize(mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.entities.categoryScrollPanel:SetPos(0, mpy * 0.075)
	function SMenu.frame.entities.categoryScrollPanel:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end

	local csx, csy = SMenu.frame.entities.categoryScrollPanel:GetSize()
	local c = 0
	for k,v in SMenu.utils.orderedPairs(SMenu.entStorage.Entities) do
		c = c + 1
		local button = SMenu.frame.entities.categoryScrollPanel:Add("SMenuButton")
		button:Dock(TOP)
		button:SetSize(csx, 75)
		button:SetText(k)
		button.Color = SMenu.Color.GetColor("secondary")
		if SMenu.frame.entities_lastSelect == v || (c == 1 && SMenu.frame.entities_lastSelect == "") then
			button:SetToggle(true)
			hasToggle = true
			SMenu.frameFunc.entities.SetEntities(v)
		end
		function button:DoClick()
			for _,v in pairs(SMenu.frame.entities.categoryScrollPanel:GetCanvas():GetChildren()) do
				v:SetToggle(false)
			end
			button:SetToggle(true)
			SMenu.frame.entities_lastSelect = v
			SMenu.frameFunc.entities.SetEntities(v)
		end
	end
end

function SMenu.frameFunc.entities.Close()
	if IsValid(SMenu.frame.entities.categoryIconBrowser) then
		SMenu.frame.entities.categoryIconBrowser:Remove()
	end

	if IsValid(SMenu.frame.entities.categoryScrollPanel) then
		SMenu.frame.entities.categoryScrollPanel:Remove()
	end
end

function SMenu.frameFunc.entities.SetEntities(entities)
	SMenu.frame.entities.categoryIconBrowser:Clear()
	for _, v in pairs(entities) do
		local icon = SMenu.frame.entities.categoryIconBrowser:Add("ContentIcon")
		icon:SetContentType( "vehicle" )
		icon:SetSpawnName( v.SpawnName )
		icon:SetName( v.PrintName or v.ClassName )
		icon:SetMaterial( "entities/" .. v.SpawnName .. ".png" )
		icon:SetAdminOnly( v.AdminOnly or false )
		function icon:DoClick()
			surface.PlaySound("ui/buttonclickrelease.wav")
			RunConsoleCommand( "gm_spawnsent", v.SpawnName )
		end
	end
end
SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_category = SMenu.frame.config_category or {}
SMenu.frame.config_category.cat = SMenu.frame.config_category.cat or {}

function SMenu.frame.config_category.showProps(closeCallback)

	local startCat = table.Copy(SMenu.frame.config_category.cat["props"])
	local popup = vgui.Create("SFrame")
	popup:SetTitle(SMenu.Language["Props"])
	popup:Center()
	popup:MakePopup()

	local sizeW, sizeH = popup:GetSize()

	local listView = vgui.Create("SListView", popup)
	listView:SetPos(sizeW * 0.1, sizeH * 0.12)
	listView:SetSize(sizeW * 0.5, sizeH * 0.7)
	listView:AddColumn(SMenu.Language["Sort"])

	function listView.Reload()
		listView:Clear()
		for _, v in pairs(SMenu.frame.config_category.cat["props"]) do
			listView:AddLine(v)
		end
	end

	local modelPanel = vgui.Create("ModelImage", popup)
	modelPanel:SetPos(sizeW * 0.62, sizeH * 0.6)
	modelPanel:SetSize(sizeW * 0.2, sizeH * 0.2)
	modelPanel:SetModel("")
	modelPanel:Hide()

	function listView:OnRowSelected(_, panel)
		modelPanel:Show()
		modelPanel:SetModel(panel:GetColumnText(1))
	end

	listView.Reload()

	local saveButton = vgui.Create("SMenuButton", popup)
	saveButton.HoverColor = SMenu.Color.GetColor("green")
	saveButton:SetPos(sizeW * 0.1, sizeH * 0.85)
	saveButton:SetSize(sizeW * 0.82, sizeH * 0.1)
	saveButton:SetText(SMenu.Language["Save"])

	function saveButton.DoClick()
		popup:IsKeyboardInputEnabled(false)
		popup:IsMouseInputEnabled(false)

		SMenu.frame.config_category.cat["props"] = {}

		for _,v in pairs(listView:GetLines()) do
			table.insert(SMenu.frame.config_category.cat["props"], v:GetColumnText(1))
		end

		SMenu.frame.config.categoryconfig.ModifyCategory(SMenu.frame.config_category.cat["name"], SMenu.frame.config_category.cat)

		SMenu.frame.popup.Info( SMenu.Language["SuccessSave"] ,function () 
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end)
		startCat = table.Copy(SMenu.frame.config_category.cat["props"])
		SMenu.frame.config_category.RefreshValue()
	end

	local addButton = vgui.Create("SMenuButton", popup)
	addButton.HoverColor = SMenu.Color.GetColor("green")
	addButton:SetPos(sizeW * 0.62, sizeH * 0.12)
	addButton:SetSize(sizeW * 0.3, sizeH * 0.1)
	addButton:SetText(SMenu.Language["Add"])

	function addButton.DoClick()
		popup:IsKeyboardInputEnabled(false)
		popup:IsMouseInputEnabled(false)

		SMenu.frame.popup.PropPopup(function(res)

			for _, v in pairs(listView:GetLines()) do
				if(v:GetColumnText(1) == res) then
					SMenu.frame.popup.Info(SMenu.Language["AlreadyExist"], closeCallback)
					popup:IsKeyboardInputEnabled(true)
					popup:IsMouseInputEnabled(true)
					return
				end
			end

			listView:AddLine(res)
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end, function () 
			popup:IsKeyboardInputEnabled(true)
			popup:IsMouseInputEnabled(true)
		end)
	end

	local removeButton = vgui.Create("SMenuButton", popup)
	removeButton.HoverColor = SMenu.Color.GetColor("red")
	removeButton:SetPos(sizeW * 0.62, sizeH * 0.24)
	removeButton:SetSize(sizeW * 0.3, sizeH * 0.1)
	removeButton:SetText(SMenu.Language["Remove"])

	function removeButton.DoClick()
		modelPanel:Hide()
		for k,v in pairs(listView:GetLines()) do
			if(table.HasValue(listView:GetSelected(), v)) then
				listView:RemoveLine(k)
			end
		end
	end
	local moveUp = vgui.Create("SMenuButton", popup)
	moveUp:SetPos(sizeW * 0.62, sizeH * 0.36)
	moveUp:SetSize(sizeW * 0.3, sizeH * 0.1)
	moveUp:SetText(SMenu.Language["MoveUp"])

	local function copy(var)
		local t = var
		return t
	end

	function moveUp.DoClick()
		for k,v in pairs(listView:GetLines()) do
			if(table.HasValue(listView:GetSelected(), v)) then
				if(k <= 0) then
					break
				end
				local temp = SMenu.frame.config_category.cat["props"][k - 1]
				SMenu.frame.config_category.cat["props"][k - 1] = SMenu.frame.config_category.cat["props"][k]
				SMenu.frame.config_category.cat["props"][k] = temp
			end
		end
		listView.Reload()
	end

	local moveDown = vgui.Create("SMenuButton", popup)
	moveDown:SetPos(sizeW * 0.62, sizeH * 0.48)
	moveDown:SetSize(sizeW * 0.3, sizeH * 0.1)
	moveDown:SetText(SMenu.Language["MoveDown"])

	function moveUp.DoClick()
		for k,v in pairs(listView:GetLines()) do
			if(table.HasValue(listView:GetSelected(), v)) then
				if(k <= 1) then
					continue
				end
				local temp = SMenu.frame.config_category.cat["props"][k - 1]
				SMenu.frame.config_category.cat["props"][k - 1] = SMenu.frame.config_category.cat["props"][k]
				SMenu.frame.config_category.cat["props"][k] = temp
			end
		end
		listView.Reload()
	end

	function moveDown.DoClick()
		for k = table.Count(listView:GetLines()), 1, -1 do
			if(table.HasValue(listView:GetSelected(), listView:GetLines()[k])) then
				if(k >= table.Count(listView:GetLines())) then
					continue
				end
				local temp = SMenu.frame.config_category.cat["props"][k + 1]
				SMenu.frame.config_category.cat["props"][k + 1] = SMenu.frame.config_category.cat["props"][k]
				SMenu.frame.config_category.cat["props"][k] = temp
			end
		end
		listView.Reload()
	end

	function popup:OnClose()
		SMenu.frame.config_category.cat["props"] = startCat
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			SMenu.frame.config_category.cat["props"] = startCat
			closeCallback()
		end
	end
end
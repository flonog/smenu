SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_tab = SMenu.frame.config_tab or {}
SMenu.frame.config_tab.tab = {}

function SMenu.frame.config_tab.Show()

	SMenu.frame.config_tab.panel = vgui.Create("SScrollPanel")
	local scrW, scrH = ScrW(), ScrH()

	-- TABS LIST

	SMenu.frame.config_tab.catCombo = vgui.Create("SComboBox", SMenu.frame.config_tab.panel)
	SMenu.frame.config_tab.catCombo:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_tab.catCombo:SetPos(scrW * 0.02, scrW * 0.03)

	SMenu.frame.config_tab.RefreshComboBox()

	function SMenu.frame.config_tab.catCombo:OnSelect(_ , _, data)
		SMenu.frame.config_tab.tab = data
		SMenu.frame.config_tab.RefreshValue()
	end
	SMenu.frame.config_tab.catLabel = vgui.Create("SLabel", SMenu.frame.config_tab.panel)
	SMenu.frame.config_tab.catLabel:SetText(SMenu.Language["Category"])
	SMenu.frame.config_tab.catLabel:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_tab.catLabel:SetPos(scrW * 0.02, scrW * 0.002)

	-- USER

	SMenu.frame.config_tab.userLabel = vgui.Create("SLabel", SMenu.frame.config_tab.panel)
	SMenu.frame.config_tab.userLabel:SetText(SMenu.Language["UserGroup"])
	SMenu.frame.config_tab.userLabel:SetSize(scrW * 0.2, scrH * 0.05)
	SMenu.frame.config_tab.userLabel:SetPos(scrW * 0.02, scrW * 0.06)

	SMenu.frame.config_tab.userNumber = vgui.Create("SLabel", SMenu.frame.config_tab.panel)
	SMenu.frame.config_tab.userNumber:SetSize(scrW * 0.3, scrH * 0.05)
	SMenu.frame.config_tab.userNumber:SetPos(scrW * 0.02, scrW * 0.072)
	SMenu.frame.config_tab.userNumber:SetFont("SMenu:Text")

	SMenu.frame.config_tab.openUserMenu = vgui.Create("SMenuButton", SMenu.frame.config_tab.panel)
	SMenu.frame.config_tab.openUserMenu:SetText(SMenu.Language["EditUserGroups"])
	SMenu.frame.config_tab.openUserMenu:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_tab.openUserMenu:SetPos(scrW * 0.02, scrW * 0.095)

	function SMenu.frame.config_tab.openUserMenu.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.config_tab.showUsergroup(function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_tab.RefreshValue()
	return SMenu.frame.config_tab.panel

end

function SMenu.frame.config_tab.RefreshComboBox()
	if IsValid(SMenu.frame.config_tab.panel) then
		SMenu.frame.config_tab.catCombo:AddChoice(SMenu.Language["Props"], SMenu.frame.config.tabs.tabs["props"])
		SMenu.frame.config_tab.catCombo:AddChoice(SMenu.Language["Entities"], SMenu.frame.config.tabs.tabs["entities"])
		SMenu.frame.config_tab.catCombo:AddChoice(SMenu.Language["Vehicles"], SMenu.frame.config.tabs.tabs["vehicles"])
		SMenu.frame.config_tab.catCombo:AddChoice(SMenu.Language["Weapons"], SMenu.frame.config.tabs.tabs["weapons"])
	
		if table.Count(SMenu.frame.config.categoryconfig.categories) > 0 then
			SMenu.frame.config_tab.catCombo:ChooseOptionID(1)
			SMenu.frame.config_tab.tab = SMenu.frame.config.tabs.tabs["props"]
		end
	end
end

function SMenu.frame.config_tab.RefreshValue()
	if IsValid(SMenu.frame.config_tab.panel) then
		SMenu.frame.config_tab.userNumber:SetText(string.format(SMenu.Language["NumberUserGroupTab"],  table.Count(SMenu.frame.config_tab.tab)))
	end
end

function SMenu.frame.config_tab.Close()
	if IsValid(SMenu.frame.config_tab.panel) then
		SMenu.frame.config_tab.panel:Remove()
	end
end

function SMenu.frame.config_tab.IsOpen()
	return IsValid(SMenu.frame.config_tab.panel)
end
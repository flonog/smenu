SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frame.tool_selected = ""
local ScreenW, ScreenH = ScrW(), ScrH()

function SMenu.frameFunc.tool()
	SMenu.frame.tool_panel = vgui.Create("EditablePanel")
	SMenu.frame.tool_panel:SetSize(ScreenW * 0.255, ScreenH * 0.9)
	SMenu.frame.tool_panel:CenterHorizontal(0.8525)
	SMenu.frame.tool_panel:CenterVertical()
	SMenu.frame.tool_panel:SetAlpha(0)
	SMenu.frame.tool_panel:AlphaTo(255, 0.1)
	SMenu.frame.tool_panel:SetKeyboardInputEnabled( true )
	SMenu.frame.tool_panel:Show()
	SMenu.frame.tool_panel:MakePopup()
	local _, ParH = SMenu.frame.main_panel:GetSize()
	
	function SMenu.frame.tool_panel.Paint(s,w,h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
		draw.RoundedBox(0, 0, 0, w, ParH * 0.075, SMenu.Color.GetColor("black"))
	end
	
	function SMenu.frame.tool_panel.Close()
		SMenu.frame.tool_panel:AlphaTo(0, 0.1, 0)
	end

	local _,TabsH = SMenu.frame.main_panel:GetSize()

	SMenu.frame.tool_list = vgui.Create("SToolMenu", SMenu.frame.tool_panel)
	SMenu.frame.tool_list:Dock(FILL)
	SMenu.frame.tool_list:SetAlpha(0)
	SMenu.frame.tool_list:AlphaTo(255, 0.1)
	SMenu.frame.tool_list:SetKeyboardInputEnabled( true )
	SMenu.frame.tool_list.Tabsize = TabsH * 0.075
	SMenu.frame.tool_list:LoadTools()
	CreateContextMenu()

end
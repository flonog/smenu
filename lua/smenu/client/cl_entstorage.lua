SMenu.vehicles = SMenu.vehicles or {}
SMenu.entStorage = SMenu.entStorage or {}

SMenu.entStorage.Vehicles = SMenu.entStorage.Vehicles or {}
function SMenu.RegisterVehicles()
	local vehicles = list.Get("Vehicles")
	for k, v in pairs(vehicles) do
		v.ClassName = k
		v.Category = v.Category or SMenu.Language["Others"]
		if SMenu.entStorage.Vehicles[v.Category] then
			table.insert(SMenu.entStorage.Vehicles[v.Category], v) 
		else
			SMenu.entStorage.Vehicles[v.Category] = { v }
		end
	end
	table.sort(SMenu.entStorage.Vehicles, function(a, b) return table.GetFirstValue(a).Category > table.GetFirstValue(b).Category end)
end

SMenu.entStorage.Weapons = SMenu.entStorage.Weapons or {}
function SMenu.RegisterWeapons()
	local Weapon = weapons.GetList()
	for k, v in pairs(Weapon) do
		if not v.Spawnable then continue end
		v.Category = v.Category or SMenu.Language["Others"]
		if SMenu.entStorage.Weapons[v.Category] then
			table.insert(SMenu.entStorage.Weapons[v.Category], v) 
		else
			SMenu.entStorage.Weapons[v.Category] = { v }
		end
	end
	table.sort(SMenu.entStorage.Weapons, function(a, b) return table.GetFirstValue(a).Category > table.GetFirstValue(b).Category end)
end

SMenu.entStorage.Entities = SMenu.entStorage.Entities or {}
function SMenu.RegisterEnts()
	local SpawnableEntities = list.Get( "SpawnableEntities" )
	for k, v in pairs(SpawnableEntities) do
		v.SpawnName = k
		v.Category = v.Category or SMenu.Language["Others"]
		if SMenu.entStorage.Entities[v.Category] then
			table.insert(SMenu.entStorage.Entities[v.Category], v) 
		else
			SMenu.entStorage.Entities[v.Category] = { v }
		end
	end
	table.sort(SMenu.entStorage.Entities, function(a, b) return table.GetFirstValue(a).Category > table.GetFirstValue(b).Category end)
end
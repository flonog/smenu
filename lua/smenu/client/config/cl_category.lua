SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.categoryconfig = SMenu.frame.config.categoryconfig or {}
SMenu.frame.config.categoryconfig.categories = {}

function SMenu.frame.config.categoryconfig.AddCategory(name)
	if SMenu.frame.config.categoryconfig.GetCategory(name) != nil then
		return
	end
	local catConfig = {
		name = name,
		props = {"models/props_borealis/bluebarrel001.mdl"},
		usergroup = {"*"},
	}
	table.insert(SMenu.frame.config.categoryconfig.categories, catConfig)
	SMenu.frame.config.categoryconfig.SendToGlobalConfig()
end

function SMenu.frame.config.categoryconfig.ModifyCategory(name, value)
	local k = SMenu.frame.config.categoryconfig.GetCategoryKey(name)
	SMenu.frame.config.categoryconfig.categories[k].props = value.props
	SMenu.frame.config.categoryconfig.categories[k].name = value.name
	SMenu.frame.config.categoryconfig.categories[k].usergroup = value.usergroup
	SMenu.frame.config.categoryconfig.SendToGlobalConfig()
end

function SMenu.frame.config.categoryconfig.GetCategory(name)
	for k,v in pairs(SMenu.frame.config.categoryconfig.categories) do
		if v.name == name then
			return v
		end
	end
	return nil
end

function SMenu.frame.config.categoryconfig.GetCategoryKey(name)
	for k,v in pairs(SMenu.frame.config.categoryconfig.categories) do
		if v.name == name then
			return k
		end
	end
end

function SMenu.frame.config.categoryconfig.Reload()
	SMenu.frame.config.categoryconfig.categories = SMenu.ConfigManager.GetConfig("categories")
end

function SMenu.frame.config.categoryconfig.SendToGlobalConfig()
	SMenu.ConfigManager.ModifyConfig("categories", SMenu.frame.config.categoryconfig.categories)
end

function SMenu.frame.config.categoryconfig.RemoveCategory(name)
	for _,v in pairs(SMenu.frame.config.categoryconfig.categories) do
		if v.name == name then
			table.remove(SMenu.frame.config.categoryconfig.categories, k)
			break
		end
	end
	SMenu.frame.config.categoryconfig.SendToGlobalConfig()
end

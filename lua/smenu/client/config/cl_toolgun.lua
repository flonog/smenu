SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.toolgun = SMenu.frame.config.toolgun or {}
SMenu.frame.config.toolgun.toolgun = SMenu.frame.config.toolgun.toolgun or {}

function SMenu.frame.config.toolgun.AddToolgun(name)
	if SMenu.frame.config.toolgun.toolgun[name] != nil then
		return
	end
	SMenu.frame.config.toolgun.toolgun[name] = {"*"}
	SMenu.frame.config.toolgun.SendToGlobalConfig()
end

function SMenu.frame.config.toolgun.ModifyToolgun(name, value)
	SMenu.frame.config.toolgun.toolgun[name] = value
end

function SMenu.frame.config.toolgun.Reload()
	SMenu.frame.config.toolgun.toolgun = SMenu.ConfigManager.GetConfig("toolgun")
end

function SMenu.frame.config.toolgun.SendToGlobalConfig()
	SMenu.ConfigManager.ModifyConfig("toolgun", SMenu.frame.config.toolgun.toolgun)
end

function SMenu.frame.config.toolgun.RemoveToolgun(name)
	local buffer = {}
	for k, v in pairs(SMenu.frame.config.toolgun.toolgun) do
		if k == name then
			continue
		end
		buffer[k] = v
	end
	SMenu.frame.config.toolgun.toolgun = buffer
	SMenu.frame.config.toolgun.SendToGlobalConfig()
end

function SMenu.frame.config.toolgun.IsAllowed(name)
	if(SMenu.Config.ShowToolIfIsVerified && table.HasValue(SMenu.Config.IgnoreToolVerification, LocalPlayer():GetUserGroup())) then
		return true
	end

	if SMenu.frame.config.toolgun.toolgun[name] == nil then
		return false
	end

	return table.HasValue(SMenu.frame.config.toolgun.toolgun[name], LocalPlayer():GetUserGroup()) || table.HasValue(SMenu.frame.config.toolgun.toolgun[name], "*")
end

function SMenu.frame.config.toolgun.GetFormatedToolgun()
	local formatedToolGun = {}
	for _,catT in pairs(spawnmenu.GetTools()[1].Items) do
		for k, val in pairs(catT) do
			if k == "Text" or k == "ItemName" then
				continue
			elseif not SMenu.frame.config.toolgun.toolgun[val.ItemName] then
				print(val.ItemName .. " n'existe pas.")
				continue
			elseif !SMenu.frame.config.toolgun.IsAllowed(val.ItemName) then
				print(val.ItemName .. " Permission Denied. (" .. tostring(table.HasValue(SMenu.frame.config.toolgun.toolgun[val.ItemName], LocalPlayer():GetUserGroup())) .. " | " .. tostring(table.HasValue(SMenu.frame.config.toolgun.toolgun[val.ItemName], "*")) ..")")
				continue
			elseif formatedToolGun[catT.Text] then
				table.insert(formatedToolGun[catT.Text], val)
			else
				formatedToolGun[catT.Text] = {val}
			end
		end
	end

	return formatedToolGun
end
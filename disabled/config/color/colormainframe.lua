SMenu.frame.config.color = SMenu.frame.config.color or {}


function SMenu.frame.config.color.Show()

	SMenu.frame.config.color.colormainFrame = vgui.Create("SFrame")
	SMenu.frame.config.color.colormainFrame:MakePopup()
	SMenu.frame.config.color.colormainFrame:SetTitle(SMenu.Language["Colors"])
	SMenu.frame.config.color.colormainFrame:SetSize(400, 600)
	SMenu.frame.config.color.colormainFrame:Center()

	SMenu.frame.config.color.colorpanel = vgui.Create("SColorMixer", SMenu.frame.config.color.colormainFrame)
	SMenu.frame.config.color.colorpanel:SetAlphaBar(false)
	SMenu.frame.config.color.colorpanel:SetColor(SMenu.Color.GetAccentColor())
	SMenu.frame.config.color.colorpanel:CenterHorizontal()
	SMenu.frame.config.color.colorpanel:CenterVertical(0.39)

	local sizex, sizey = SMenu.frame.config.color.colormainFrame:GetSize()

	SMenu.frame.config.color.colorLabel = vgui.Create("DLabel", SMenu.frame.config.color.colormainFrame)
	SMenu.frame.config.color.colorLabel:SetSize(sizex * 0.9, sizey * 0.15)
	SMenu.frame.config.color.colorLabel:CenterHorizontal(0.5)
	SMenu.frame.config.color.colorLabel:CenterVertical(0.14)
	SMenu.frame.config.color.colorLabel:SetText(SMenu.Language["Accentuation"])
	SMenu.frame.config.color.colorLabel:SetFont("SMenu:Title")
	SMenu.frame.config.color.colorLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config.color.preview1 = vgui.Create("DPanel",  SMenu.frame.config.color.colormainFrame)
	SMenu.frame.config.color.preview1:SetSize(sizex * 0.40, sizey * 0.05)
	SMenu.frame.config.color.preview1:CenterHorizontal(0.75)
	SMenu.frame.config.color.preview1:CenterVertical(0.65)

	function SMenu.frame.config.color.preview1:Paint(w, h)
		local color =  SMenu.Color.AdditionColor(SMenu.frame.config.color.colorpanel:GetColor(), SMenu.Color["accent"])
		draw.RoundedBox(0, 0, 0, w, h, color)
	end

	SMenu.frame.config.color.preview2 = vgui.Create("DPanel", SMenu.frame.config.color.colormainFrame) 
	SMenu.frame.config.color.preview2:SetSize(sizex * 0.40, sizey * 0.05)
	SMenu.frame.config.color.preview2:CenterHorizontal(0.25)
	SMenu.frame.config.color.preview2:CenterVertical(0.65)

	function SMenu.frame.config.color.preview2:Paint(w, h)
		local color =  SMenu.Color.AdditionColor(SMenu.frame.config.color.colorpanel:GetColor(), SMenu.Color["accentdark"])
		draw.RoundedBox(0, 0, 0, w, h, color)
	end

	SMenu.frame.config.color.themeLabel = vgui.Create("DLabel", SMenu.frame.config.color.colormainFrame)
	SMenu.frame.config.color.themeLabel:SetSize(sizex * 0.40, sizey * 0.065)
	SMenu.frame.config.color.themeLabel:CenterHorizontal(0.15)
	SMenu.frame.config.color.themeLabel:CenterVertical(0.75)
	SMenu.frame.config.color.themeLabel:SetText(SMenu.Language["Theme"])
	SMenu.frame.config.color.themeLabel:SetContentAlignment(5)
	SMenu.frame.config.color.themeLabel:SetFont("SMenu:Text")
	SMenu.frame.config.color.themeLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config.color.theme = vgui.Create("SComboBox", SMenu.frame.config.color.colormainFrame) 
	SMenu.frame.config.color.theme:SetSize(sizex * 0.6, sizey * 0.065)
	SMenu.frame.config.color.theme:CenterHorizontal(0.6)
	SMenu.frame.config.color.theme:CenterVertical(0.75)
	SMenu.frame.config.color.theme:SetFont("SMenu:Text")

	SMenu.frame.config.color.theme:AddChoice(SMenu.Language["Dark"], "dark", true)
	SMenu.frame.config.color.theme:AddChoice(SMenu.Language["Light"], "light", SMenu.Color.IsLightTheme())

	SMenu.frame.config.color.buttonsave = vgui.Create("SMenuButton", SMenu.frame.config.color.colormainFrame)
	SMenu.frame.config.color.buttonsave.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.color.buttonsave:SetSize(sizex * 0.5, sizey * 0.075)
	SMenu.frame.config.color.buttonsave:CenterHorizontal(0.5)
	SMenu.frame.config.color.buttonsave:CenterVertical(0.9)
	SMenu.frame.config.color.buttonsave:SetText(SMenu.Language["Save"])

	function SMenu.frame.config.color.buttonsave.DoClick()
		SMenu.frame.config.color.colormainFrame:Close()

		local color = SMenu.frame.config.color.colorpanel:GetColor()
		local _, str = SMenu.frame.config.color.theme:GetSelected()
		SMenu.Color.SetAccentColor(color.r, color.g, color.b)
		SMenu.Color.SetIsLightTheme(str == "light")
		SMenu.Color.SendToGlobalConfig()
		SMenu.Color.LoadColors()

		SMenu.frame.popup.Info(SMenu.Language["SuccessSave"], function()
			SMenu.frame.config.color.Show()
		end)
	end
end
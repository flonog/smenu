SMenu = SMenu or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.toolgun = SMenu.frame.config.toolgun or {}

function SMenu.frame.config.toolgun.ReloadUserGroup(toolgun)
	SMenu.frame.config.toolgun.usergroupList:Clear()
	if SMenu.frame.config.toolgun.toolgun[toolgun] == nil then
		SMenu.frame.config.toolgun.toolgun[toolgun] = {"*"}
	end

	for _,v in pairs(SMenu.frame.config.toolgun.comboboxFrame:GetDataByChoice(toolgun)) do
		SMenu.frame.config.toolgun.usergroupList:AddLine(v)
	end
end

function SMenu.frame.config.toolgun.ReloadChoice()
	SMenu.frame.config.toolgun.comboboxFrame:Clear()
	for k,v in pairs(SMenu.frame.config.toolgun.toolgun) do
		SMenu.frame.config.toolgun.comboboxFrame:AddChoice(k, v, true)
		SMenu.frame.config.toolgun.comboboxFrame:SetValue(k)
		SMenu.frame.config.toolgun.comboboxFrame:OnSelect(0, k, v)
	end
end

function SMenu.frame.config.toolgun.Show()
	SMenu.frame.config.toolgun.Reload()
	
	SMenu.frame.config.toolgun.toolgunmainFrame = vgui.Create("SFrame")
	SMenu.frame.config.toolgun.toolgunmainFrame:MakePopup()
	SMenu.frame.config.toolgun.toolgunmainFrame:SetTitle(SMenu.Language["Tool"])
	SMenu.frame.config.toolgun.toolgunmainFrame:SetSize(ScrW()*0.4, ScrH()*0.8)
	SMenu.frame.config.toolgun.toolgunmainFrame:Center()

	SMenu.frame.config.toolgun.usergroupList = vgui.Create("SListView", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.usergroupList:SetSize(ScrH()* 0.8 * 0.35, ScrW()* 0.8 * 0.3)
	SMenu.frame.config.toolgun.usergroupList:CenterHorizontal()
	SMenu.frame.config.toolgun.usergroupList:CenterVertical(0.55)
	SMenu.frame.config.toolgun.usergroupList:AddColumn(SMenu.Language["Sort"])
	
	SMenu.frame.config.toolgun.comboboxFrame = vgui.Create("SComboBox", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.comboboxFrame:SetSize(ScrW()* 0.8 * 0.3, ScrH() *0.8 * 0.1)
	SMenu.frame.config.toolgun.comboboxFrame:CenterHorizontal()
	SMenu.frame.config.toolgun.comboboxFrame:CenterVertical(0.2)

	SMenu.frame.config.toolgun.addToolButton = vgui.Create("SMenuButton", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.addToolButton:SetSize( ScrH()* 0.8 * 0.05,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.toolgun.addToolButton:CenterHorizontal(0.8)
	SMenu.frame.config.toolgun.addToolButton:CenterVertical(0.2)
	SMenu.frame.config.toolgun.addToolButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.toolgun.addToolButton:SetText("＋")

	function SMenu.frame.config.toolgun.addToolButton:DoClick()
		SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.ToolgunPopup(function (res, pnl)
			if(IsValid(SMenu.frame.config.toolgun.comboboxFrame)) then
				SMenu.frame.config.toolgun.ReloadChoice()
				SMenu.frame.config.toolgun.AddToolgun(res)
				SMenu.frame.config.toolgun.SendToGlobalConfig()
				SMenu.frame.config.toolgun.comboboxFrame:AddChoice(res, {"*"}, true)
				SMenu.frame.config.toolgun.comboboxFrame:SetValue(res)
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, SMenu.frame.config.toolgun.toolgun, function ()
			SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.toolgun.removeToolButton = vgui.Create("SMenuButton", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.removeToolButton:SetSize( ScrH()* 0.8 * 0.05,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.toolgun.removeToolButton:CenterHorizontal(0.2)
	SMenu.frame.config.toolgun.removeToolButton:CenterVertical(0.2)
	SMenu.frame.config.toolgun.removeToolButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.toolgun.removeToolButton:SetText("−")
	function SMenu.frame.config.toolgun.removeToolButton:DoClick()
		local sel,_ = SMenu.frame.config.toolgun.comboboxFrame:GetSelected()
		if(sel != nil) then
			SMenu.frame.config.toolgun.toolgun[sel] = nil
			SMenu.frame.config.toolgun.comboboxFrame:Clear()
			SMenu.frame.config.toolgun.ReloadChoice()
		end
	end
	
	function SMenu.frame.config.toolgun.comboboxFrame:OnSelect(index, value, data)
		SMenu.frame.config.toolgun.ReloadUserGroup(value)
	end

	SMenu.frame.config.toolgun.ReloadChoice()

	SMenu.frame.config.toolgun.addButton = vgui.Create("SMenuButton", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.addButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.toolgun.addButton:CenterHorizontal(0.25)
	SMenu.frame.config.toolgun.addButton:CenterVertical(0.87)
	SMenu.frame.config.toolgun.addButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.toolgun.addButton:SetText(SMenu.Language["Add"])
	function SMenu.frame.config.toolgun.addButton:DoClick()
		SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.AskText(SMenu.Language["AskUserGroup"], function (res, pnl)
			if(IsValid(SMenu.frame.config.toolgun.comboboxFrame)) then
				SMenu.frame.config.toolgun.usergroupList:AddLine(res)
				local selected = SMenu.frame.config.toolgun.comboboxFrame:GetSelected()
				local data = SMenu.frame.config.toolgun.comboboxFrame:GetDataByChoice(selected)
				table.insert(data, res)
				SMenu.frame.config.toolgun.comboboxFrame:SetDataChoice(selected, data)
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function ()
			SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.toolgun.removeButton = vgui.Create("SMenuButton", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.removeButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.toolgun.removeButton:CenterHorizontal(0.75)
	SMenu.frame.config.toolgun.removeButton:CenterVertical(0.87)
	SMenu.frame.config.toolgun.removeButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.toolgun.removeButton:SetText(SMenu.Language["Remove"])
	function SMenu.frame.config.toolgun.removeButton:DoClick()
		local selectedLines = SMenu.frame.config.toolgun.usergroupList:GetSelected()
		local tool = SMenu.frame.config.toolgun.comboboxFrame:GetSelected()
		local data = SMenu.frame.config.toolgun.comboboxFrame:GetDataByChoice(tool)
		for _, v in pairs(selectedLines) do
			table.RemoveByValue(SMenu.frame.config.toolgun.usergroupList.Lines, v)
			table.RemoveByValue(SMenu.frame.config.toolgun.usergroupList.Sorted, v )
			table.RemoveByValue(data, v:GetColumnText(1))

			SMenu.frame.config.toolgun.usergroupList:SetDirty( true )
			SMenu.frame.config.toolgun.usergroupList:InvalidateLayout()

			v:Remove()
		end
		SMenu.frame.config.toolgun.comboboxFrame:SetDataChoice(tool, data)
	end

	SMenu.frame.config.toolgun.saveButton = vgui.Create("SMenuButton", SMenu.frame.config.toolgun.toolgunmainFrame)
	SMenu.frame.config.toolgun.saveButton:SetSize( ScrW()* 0.8 * 0.2,ScrH()* 0.8 * 0.05)
	SMenu.frame.config.toolgun.saveButton:CenterHorizontal(0.5)
	SMenu.frame.config.toolgun.saveButton:CenterVertical(0.94)
	SMenu.frame.config.toolgun.saveButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.toolgun.saveButton:SetText(SMenu.Language["Save"])
	function SMenu.frame.config.toolgun.saveButton:DoClick()
		for k, v in pairs(SMenu.frame.config.toolgun.comboboxFrame.Choices) do
			SMenu.frame.config.toolgun.ModifyToolgun(v, SMenu.frame.config.toolgun.comboboxFrame.Data[k])
		end
		SMenu.frame.config.toolgun.SendToGlobalConfig()
		SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.Info(SMenu.Language["SuccessSave"], function()
			SMenu.frame.config.toolgun.toolgunmainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.toolgun.toolgunmainFrame:SetKeyboardInputEnabled(true)
		end)
	end
end
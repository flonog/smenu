SMenu = SMenu or {}
SMenu.frame.config.categoryoption = SMenu.frame.config.categoryoption or {}
SMenu.frame.config.categoryoption.selected = ""
SMenu.frame.config.categoryoption.selectedCatInfo = {}
SMenu.frame.config.categoryoption.copied = {}
SMenu.frame.config.categoryconfig = SMenu.frame.config.categoryconfig or {}
function SMenu.frame.config.categoryoption.Show()

	
	local mfx, mfy = SMenu.frame.config.category.categorymainFrame:GetSize()

	SMenu.frame.config.categoriesOption = vgui.Create("DPanel", SMenu.frame.config.category.categorymainFrame)
	SMenu.frame.config.categoriesOption:SetSize(mfx * 0.7 + 1, mfy - 50)
	SMenu.frame.config.categoriesOption:SetPos(mfx * 0.3, 50)
	SMenu.frame.config.categoriesOption.Paint = function(s, w, h)

	end

	local cox, coy = SMenu.frame.config.categoriesOption:GetSize()
	SMenu.frame.config.categoriesOption_deleteButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_deleteButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.categoriesOption_deleteButton:SetText(SMenu.Language["Delete"])
	SMenu.frame.config.categoriesOption_deleteButton:SetSize(cox * 0.18, cox * 0.05)
	SMenu.frame.config.categoriesOption_deleteButton:SetPos(621, coy * 0.9)
	SMenu.frame.config.categoriesOption_deleteButton:CenterHorizontal(0.7)

	SMenu.frame.config.categoriesOption_saveButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_saveButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.categoriesOption_saveButton:SetText(SMenu.Language["Save"])
	SMenu.frame.config.categoriesOption_saveButton:SetSize(cox * 0.2, cox * 0.05)
	SMenu.frame.config.categoriesOption_saveButton:SetPos(621, coy * 0.9)
	SMenu.frame.config.categoriesOption_saveButton:CenterHorizontal(0.3)

	SMenu.frame.config.categoriesOption_selectLabel = vgui.Create("SLabel", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_selectLabel:SetText(SMenu.Language["SelectCat"])
	SMenu.frame.config.categoriesOption_selectLabel:SetSize(cox * 0.25, cox * 0.05)
	SMenu.frame.config.categoriesOption_selectLabel:SetPos(0, cox * 0.005)
	SMenu.frame.config.categoriesOption_selectLabel:CenterHorizontal()
	SMenu.frame.config.categoriesOption_selectLabel:SetContentAlignment(5)
	SMenu.frame.config.categoriesOption_selectLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config.categoriesOption_nameEntry = vgui.Create("STextEntry", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_nameEntry:SetText("")
	SMenu.frame.config.categoriesOption_nameEntry:SetSize(cox * 0.25, cox * 0.05)
	SMenu.frame.config.categoriesOption_nameEntry:SetPos(0, cox * 0.05)
	SMenu.frame.config.categoriesOption_nameEntry:CenterHorizontal()

	SMenu.frame.config.categoriesOption_addPropButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_addPropButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.categoriesOption_addPropButton:SetText(SMenu.Language["Add"])
	SMenu.frame.config.categoriesOption_addPropButton:SetSize(cox * 0.15, cox * 0.05)
	SMenu.frame.config.categoriesOption_addPropButton:SetPos(621, coy * 0.825)
	SMenu.frame.config.categoriesOption_addPropButton:CenterHorizontal(0.1)

	SMenu.frame.config.categoriesOption_removePropButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_removePropButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.categoriesOption_removePropButton:SetText(SMenu.Language["Remove"])
	SMenu.frame.config.categoriesOption_removePropButton:SetSize(cox * 0.15, cox * 0.05)
	SMenu.frame.config.categoriesOption_removePropButton:SetPos(621, coy * 0.825)
	SMenu.frame.config.categoriesOption_removePropButton:CenterHorizontal(0.3)

	SMenu.frame.config.categoriesOption_propList = vgui.Create("SListView", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_propList:SetSize(cox * 0.3, coy * 0.55)
	SMenu.frame.config.categoriesOption_propList:SetPos(621, coy * 0.25)
	SMenu.frame.config.categoriesOption_propList:CenterHorizontal(0.2)
	SMenu.frame.config.categoriesOption_propList:AddColumn(SMenu.Language["Sort"])

	SMenu.frame.config.categoriesOption_propLabel = vgui.Create("SLabel", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_propLabel:SetText(SMenu.Language["Props"])
	SMenu.frame.config.categoriesOption_propLabel:SetSize(cox * 0.25, cox * 0.05)
	SMenu.frame.config.categoriesOption_propLabel:SetPos(0, cox * 0.125)
	SMenu.frame.config.categoriesOption_propLabel:CenterHorizontal(0.2)
	SMenu.frame.config.categoriesOption_propLabel:SetContentAlignment(5)
	SMenu.frame.config.categoriesOption_propLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config.categoriesOption_addUserGroupButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_addUserGroupButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.categoriesOption_addUserGroupButton:SetText(SMenu.Language["Add"])
	SMenu.frame.config.categoriesOption_addUserGroupButton:SetSize(cox * 0.15, cox * 0.05)
	SMenu.frame.config.categoriesOption_addUserGroupButton:SetPos(621, coy * 0.825)
	SMenu.frame.config.categoriesOption_addUserGroupButton:CenterHorizontal(0.7)

	SMenu.frame.config.categoriesOption_removeUserGroupButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_removeUserGroupButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config.categoriesOption_removeUserGroupButton:SetText(SMenu.Language["Remove"])
	SMenu.frame.config.categoriesOption_removeUserGroupButton:SetSize(cox * 0.15, cox * 0.05)
	SMenu.frame.config.categoriesOption_removeUserGroupButton:SetPos(621, coy * 0.825)
	SMenu.frame.config.categoriesOption_removeUserGroupButton:CenterHorizontal(0.9)

	SMenu.frame.config.categoriesOption_userGroupList = vgui.Create("SListView", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_userGroupList:SetSize(cox * 0.3, coy * 0.55)
	SMenu.frame.config.categoriesOption_userGroupList:SetPos(621, coy * 0.25)
	SMenu.frame.config.categoriesOption_userGroupList:CenterHorizontal(0.8)
	SMenu.frame.config.categoriesOption_userGroupList:AddColumn(SMenu.Language["Sort"])

	SMenu.frame.config.categoriesOption_userGroupLabel = vgui.Create("SLabel", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_userGroupLabel:SetText(SMenu.Language["UserGroup"])
	SMenu.frame.config.categoriesOption_userGroupLabel:SetSize(cox * 0.25, cox * 0.05)
	SMenu.frame.config.categoriesOption_userGroupLabel:SetPos(0, cox * 0.125)
	SMenu.frame.config.categoriesOption_userGroupLabel:CenterHorizontal(0.8)
	SMenu.frame.config.categoriesOption_userGroupLabel:SetContentAlignment(5)
	SMenu.frame.config.categoriesOption_userGroupLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config.categoriesOption_copyButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_copyButton.HoverColor = SMenu.Color.GetColor("accent")
	SMenu.frame.config.categoriesOption_copyButton:SetText(SMenu.Language["Copy"])
	SMenu.frame.config.categoriesOption_copyButton:SetSize(cox * 0.18, cox * 0.05)
	SMenu.frame.config.categoriesOption_copyButton:CenterHorizontal()
	SMenu.frame.config.categoriesOption_copyButton:CenterVertical(0.5)


	SMenu.frame.config.categoriesOption_pasteButton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesOption)
	SMenu.frame.config.categoriesOption_pasteButton.HoverColor = SMenu.Color.GetColor("accent")
	SMenu.frame.config.categoriesOption_pasteButton:SetText(SMenu.Language["Paste"])
	SMenu.frame.config.categoriesOption_pasteButton:SetSize(cox * 0.18, cox * 0.05)
	SMenu.frame.config.categoriesOption_pasteButton:CenterHorizontal()
	SMenu.frame.config.categoriesOption_pasteButton:CenterVertical(0.6)


	SMenu.frame.config.categoryoption.UnselectCategory()
end

function SMenu.frame.config.categoryoption.SetCategoryActive(cat)
	SMenu.frame.config.categoryoption.UnselectCategory()
	SMenu.frame.config.categoryoption.selected = cat
	SMenu.frame.config.categoryoption.selectedCatInfo = SMenu.frame.config.categoryconfig.GetCategory(cat)
	SMenu.frame.config.categoriesOption_selectLabel:Hide()

	SMenu.frame.config.categoriesOption_deleteButton:Show()

	function SMenu.frame.config.categoriesOption_deleteButton:DoClick()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.Confirm(SMenu.Language["ConfirmDelete"], function (pnl)
			if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
				SMenu.frame.config.categoryconfig.RemoveCategory(cat)
				SMenu.frame.config.categoryoption.UnselectCategory()
				SMenu.frame.config.categorylist.Reload()
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function ()
			if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
				SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
				SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
			end
		end)
	end

-- SHA 256 : {{ user_id sha256 key }}
	SMenu.frame.config.categoriesOption_nameEntry:Show()
	SMenu.frame.config.categoriesOption_nameEntry:SetText(cat)

	SMenu.frame.config.categoriesOption_addPropButton:Show()
	function SMenu.frame.config.categoriesOption_addPropButton:DoClick()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.PropPopup(function(res, pnl)
			if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
				SMenu.frame.config.categoriesOption_propList:AddLine(res)
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function()
			SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.categoriesOption_removePropButton:Show()
	function SMenu.frame.config.categoriesOption_removePropButton:DoClick()
		local selectedLines = SMenu.frame.config.categoriesOption_propList:GetSelected()
		for _, v in pairs(selectedLines) do
			table.RemoveByValue(SMenu.frame.config.categoriesOption_propList.Lines, v)
			table.RemoveByValue( SMenu.frame.config.categoriesOption_propList.Sorted, v )

			SMenu.frame.config.categoriesOption_propList:SetDirty( true )
			SMenu.frame.config.categoriesOption_propList:InvalidateLayout()

			v:Remove()
		end
	end

	SMenu.frame.config.categoriesOption_propList:Show()
	for k, v in pairs(SMenu.frame.config.categoryoption.selectedCatInfo.props) do
		SMenu.frame.config.categoriesOption_propList:AddLine(v)
	end

	SMenu.frame.config.categoriesOption_propLabel:Show()

	SMenu.frame.config.categoriesOption_addUserGroupButton:Show()
	function SMenu.frame.config.categoriesOption_addUserGroupButton:DoClick()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.AskText(SMenu.Language["AskUserGroup"], function(res, pnl)
			if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
				SMenu.frame.config.categoriesOption_userGroupList:AddLine(res)
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function()
			SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.categoriesOption_removeUserGroupButton:Show()
	function SMenu.frame.config.categoriesOption_removeUserGroupButton:DoClick()
		local selectedLines = SMenu.frame.config.categoriesOption_userGroupList:GetSelected()
		for _, v in pairs(selectedLines) do
			table.RemoveByValue(SMenu.frame.config.categoriesOption_userGroupList.Lines, v)
			table.RemoveByValue( SMenu.frame.config.categoriesOption_userGroupList.Sorted, v )

			SMenu.frame.config.categoriesOption_userGroupList:SetDirty( true )
			SMenu.frame.config.categoriesOption_userGroupList:InvalidateLayout()

			v:Remove()
		end
	end

	SMenu.frame.config.categoriesOption_userGroupList:Show()
	for k, v in pairs(SMenu.frame.config.categoryoption.selectedCatInfo.usergroup) do
		SMenu.frame.config.categoriesOption_userGroupList:AddLine(v)
	end

	SMenu.frame.config.categoriesOption_userGroupLabel:Show()
	
	SMenu.frame.config.categoriesOption_saveButton:Show()
	function SMenu.frame.config.categoriesOption_saveButton:DoClick()
		local catVal = SMenu.frame.config.categoryoption.GetActualCatValue()
		if(catVal == SMenu.frame.config.categoryoption.selectedCatInfo) then
			return
		end

		SMenu.frame.config.categoryconfig.ModifyCategory(SMenu.frame.config.categoryoption.selected, catVal)
		SMenu.frame.config.categorylist.Reload()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.Info(SMenu.Language["SuccessSave"], function ()
			SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.categoriesOption_pasteButton:Show()
	function SMenu.frame.config.categoriesOption_copyButton:DoClick()
		SMenu.frame.config.categoryoption.Copy(SMenu.frame.config.categoryoption.GetActualCatValue())
	end

	SMenu.frame.config.categoriesOption_copyButton:Show()
	function SMenu.frame.config.categoriesOption_pasteButton:DoClick()

		SMenu.frame.config.categoryoption.Paste()
		
	end
end

function SMenu.frame.config.categoryoption.GetActualCatValue()
	local newProps = {}
	local newUserGroup = {}

	for _,v in pairs(SMenu.frame.config.categoriesOption_propList:GetLines()) do
		table.insert(newProps, v:GetColumnText(1))
	end

	for _,v in pairs(SMenu.frame.config.categoriesOption_userGroupList:GetLines()) do
		table.insert(newUserGroup, v:GetColumnText(1))
	end

	local newCat = {
		name = SMenu.frame.config.categoriesOption_nameEntry:GetText(),
		props = newProps,
		usergroup = newUserGroup
	}

	return newCat
end

function SMenu.frame.config.categoryoption.UnselectCategory()
	SMenu.frame.config.categoriesOption_selectLabel:Show()
	SMenu.frame.config.categoriesOption_deleteButton:Hide()
	SMenu.frame.config.categoriesOption_saveButton:Hide()
	SMenu.frame.config.categoriesOption_nameEntry:Hide()
	SMenu.frame.config.categoriesOption_addPropButton:Hide()
	SMenu.frame.config.categoriesOption_removePropButton:Hide()
	SMenu.frame.config.categoriesOption_propList:Hide()
	SMenu.frame.config.categoriesOption_propList:Clear()
	SMenu.frame.config.categoriesOption_propLabel:Hide()
	SMenu.frame.config.categoriesOption_addUserGroupButton:Hide()
	SMenu.frame.config.categoriesOption_removeUserGroupButton:Hide()
	SMenu.frame.config.categoriesOption_userGroupList:Hide()
	SMenu.frame.config.categoriesOption_userGroupList:Clear()
	SMenu.frame.config.categoriesOption_userGroupLabel:Hide()
	SMenu.frame.config.categoriesOption_pasteButton:Hide()
	SMenu.frame.config.categoriesOption_copyButton:Hide()
end

function SMenu.frame.config.categoryoption.Copy(catInfo)
	SMenu.frame.config.categoryoption.copied = catInfo
end

function SMenu.frame.config.categoryoption.Paste()
	local catInfo = SMenu.frame.config.categoryoption.copied

	if(table.IsEmpty(catInfo)) then
		return
	end

	SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
	SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)

	SMenu.frame.popup.Confirm(SMenu.Language["ConfirmPaste"], function (pnl)
		if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
			SMenu.frame.config.categoriesOption_userGroupList:Clear()
			SMenu.frame.config.categoriesOption_propList:Clear()

			for k, v in pairs(catInfo.props) do
				SMenu.frame.config.categoriesOption_propList:AddLine(v)
			end

			for k, v in pairs(catInfo.usergroup) do
				SMenu.frame.config.categoriesOption_userGroupList:AddLine(v)
			end
		else
			pnl:Close()
			SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
		end
	end, function()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
	end)
end